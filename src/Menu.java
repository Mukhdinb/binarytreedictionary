import java.util.Scanner;

public class Menu {

    public static void main(String[] args){
        Scanner keyboard = new Scanner(System.in);
        BinTree binTre;
        binTre=new BinTree(args[0]);//deklarasjon av BinTre
        binTre.read();//Leser inn og legger inn
        String m ="no choise";
        boolean b =true;
        while(b){
            System.out.println("    |Menu|");
            System.out.println("1:Find word/spellcheck");
            System.out.println("2:delete");
            System.out.println("3:insert");
            System.out.println("4:Statistikk");
            System.out.println("5:write to file");
            System.out.println("6:sum word in dictianry");
            System.out.println("q:quit");
            m=keyboard.nextLine();
            String str;
            int mengde;
            switch(m) {
                case "1"://Menu option 1  (Find word)
                    System.out.println("Write the word you want to find");
                    str=keyboard.nextLine();
                    binTre.spellCheck(str);
                    break;
                //END Menu option 1  (Find word)


                case "2"://Menu option 2  (Delete word)
                    System.out.println("Write the word you want to delete");
                    str=keyboard.nextLine();
                    boolean slettet=binTre.delete(binTre.root, null, str);
                    mengde = binTre.amountOfNodes();
                    if(slettet){
                        System.out.println("Fant ordet du lette etter og slettet. Antall ord igjen: " + mengde);
                    }else {
                        System.out.println("Fant ikke ordet du lette etter.");
                    }
                    break;
                //Slutt På menu valg 2(Slett ord)


                case "3":/*Menu valg 3 (insert) Siden jeg ikke har
		     return boolean for insert så må jeg kjekke først...
		     som sagt jeg har ikke tid på å lage den i return
		     men skal gjøre det når jeg får tid*/
                    System.out.println("Skriv inn ord du vil legge til");
                    str=keyboard.nextLine();
                    boolean ex=binTre.exists(binTre.root, str);
                    if(ex)System.out.println(str + "er allerede i ordbok");
                    else{
                        binTre.root=binTre.insert(binTre.root, str);
                        System.out.println(str + " er lagt til i ordbok");
                    }
                    break;



                case"4":
                    binTre.findDepths(binTre.root, 0);
                    binTre.findDeepest();
                    binTre.firstWord(binTre.root);
                    binTre.finalWord(binTre.root);
                    System.out.println("Antal dybder i treet er  " + binTre.arrDybde.length);
                    System.out.println("Den dypeste blad er  " + binTre.maxDybde);
                    System.out.println("Gjennomsnittelig dybde er  " + binTre.findAverageD());
                    System.out.println("Første ord i ordbok er  " + binTre.firstW);
                    System.out.println("Siste ord i ordbok er  " + binTre.finalW);

                    break;

                case "5":
                    binTre.skrivTilFil();
                    break;
                case "6"://Menu valg 8 (Antall ord)
                    mengde=binTre.amountOfNodes();
                    System.out.println("Antall ord i ordbook: " + mengde);
                    break;


                case "q":
                    b=false;
                    break;
                default:
                    System.out.println("Ugyldig valg");
                    break;
            }
        }

    }


}
