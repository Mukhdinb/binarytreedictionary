import java.io.File;
import java.io.FileWriter;
import java.util.Arrays;
import java.util.Scanner;

public class BinTree {
    Node root;
    String filNavn;
    String finalW;
    String firstW;
    int antallOrd;
    int maxDybde;
    int sumDybde;
    int antDybde;
    int[] arrDybde=new int[1];
    int size = 0;

    BinTree(String filNavn){
        this.filNavn=filNavn;
        sumDybde=0;
        antDybde=0;
    }

    //Metoden Leser inn ord og Bygger treet fra root og opp.
    void read(){
        Scanner f;
        try{
            f=new Scanner(new File(filNavn));//Opner fil
            while(f.hasNext()){
                root=insert(root, f.next());//Leser fra fil og sender til insert som legger inn
            }

            System.out.println("Initial size of dictionary: " + size);
        }catch(Exception e){
            System.out.println("feil paa read");
        }
    }// Slutt på LESOGLEGG


    /*Rekursive inserting*/
    public Node insert (Node node, String ord) {
        if(node==null) {
            size++;//ker size
            return new Node(ord);
        }
        int comp = ord.compareTo(node.verdi);
        if(comp<0){
            node.left=insert(node.left, ord);
        }else if(comp>0) {
            node.right=insert(node.right, ord);
        }
        return node;
    }//END INSERT




    //Kjekker om ord existere i treet
    public boolean exists(Node node, String ord){
        if(node==null) return false;

        int comp=ord.compareTo(node.verdi);
        //System.out.println(ord);
        if(comp<0){
            return exists(node.left, ord);
        } else if(comp>0){
            return exists(node.right, ord);
        }

        return true;
    }




    // Hjelpemetode til delete.
    public Node traverse(Node current, Node parent, boolean wentLeft) {
        // Sjekk venstre først siden det er den minste verdien
        if (current.left != null) {
            return traverse(current.left, current, true);
        }
    	/* Hvis vi ikke hadde noe venstre barn sjekker vi høyre.
	   trenger ikke if/else fordi første if har en return.
	   Det vil si at hvis vi kommer inn i første if, vil vi aldri komme inn i
	   denne if'en.*/
        if (current.right != null) {
            return traverse(current.right, current, false);
        }

        // fant noden som skal erstatte den vi slettet, men nå må vi bare sørge
        // for at foreldren til noden vi skal bruke til å erstatte må få fjernet pekeren
        // til denne noden.
        if (wentLeft) {
            parent.left = null;
        } else {
            parent.right = null;
        }

        return current;

    }//Slutt på traverse





    //Delete metode
    public boolean delete(Node current, Node parent, String slettOrd){//Hjelp Med Sletting
        // Hvis noden vi har kommet til er null betyr det at det ikke eksisterer
        // en node med det ordet brukeren ønsker å slette.
        if(current==null) {
            return false;
        }

        int comp = slettOrd.compareTo(current.verdi);
        // Navigerer oss enten til venstre eller høyre for å finne ordet.
        if (comp<0) {
            return delete(current.left, current, slettOrd);
        } else if (comp>0) {
            return delete(current.right, current, slettOrd);
        }

        // Hvis vi fant ordet vi vil slette:
        if(current==null) {
            return false;
        }else {
            // Det er 4 cases vi må ta hensyn til når du skal slette en node:

            // 1: Ingen barn (ingen venstre/høyre):
            if (current.left == null && current.right == null) {
                // Vi kan bare sette noden lik null.
                current = null;
            }
            // 2: Bare venstre barn
            else if (current.left != null && current.right == null) {
                // Current har bare ett barn (venstre). Altså må vi da få parent til
                // current til å linke til barnet til current. Vi må sjekke hvilken barn
                // (høyre eller venstre) current er til parent.
                if (parent.left == current) {
                    parent.left = current.left;
                } else {
                    parent.right = current.left;
                }

            }
            // 3: Bare høyre barn
            else if (current.left == null && current.right != null) {
                // Samme greia her, bare høyre istedenfor venstre.
                if (parent.left == current) {
                    parent.left = current.right;
                } else {
                    parent.right = current.right;
                }
            }
            // 4: Har to barn (venstre og høyre)
            else {

                Node replacementNode = null;
                // Vi må traversere nedover treet og finne en node som ikke har noen barn
                // og kan erstatte current.
                if (parent.left == current) {
                    //
                    replacementNode = traverse(current.right, current, true);
                } else {
                    // parent.right = current.right;
                    replacementNode = traverse(current.left, current, false);
                }

                // Siden replacementNode skal erstatte current, må current sine barn overføres til
                // replacementNode.
                replacementNode.left = current.left;
                replacementNode.right = current.right;

                // Foreldren til current må vite at den skal peke til replacementNode nå.
                if (parent.left == current) {
                    parent.left = replacementNode;
                }
                else {
                    parent.right = replacementNode;
                }
                // Current kan setter til null nå og vil bli slettet av garbage collectoren.
                current = null;
            }
            // Reduserer antall noder med 1.
            size--;
            return true;
        }


    }//Slutt på delete


    //Metoden similarOne som vi fikk for å kjekke første X fra Oblig1
    public String[] similarOne(String word){
        char[] word_array = word.toCharArray();
        char[] tmp;
        String[] words = new String[word_array.length-1];
        for(int i = 0; i < word_array.length - 1; i++){
            tmp = word_array.clone();
            words[i] = swap(i, i+1, tmp);
        }
        return words;
    }
    public String swap(int a, int b, char[] word){
        char tmp = word[a];
        word[a] = word[b];
        word[b] = tmp;
        return new String(word);
    }



    //Simular 2 andre X
    public String[][] similarTwo(String word){
        char[] alphabet = "abcdefghijklmnopqrstuvwxyzæøå".toCharArray();
        String str_alphabet = new String(alphabet);
        char[] word_array = word.toCharArray();
        char[] tmp;
        String[][] words = new String[word_array.length][alphabet.length-1];
        for(int i = 0; i < word_array.length; i++){
            for(int j=0; j< alphabet.length -1; j++){
                tmp = word_array.clone();
                words[i][j] = replace1(i, alphabet[j], tmp);
            }
        }
        return words;
    }
    public String replace1(int a, char b, char [] word){
        if(word[a]!=b) word[a]=b;
        return new String (word);
    }//Slutt Similar2





    //similar3 Tredje X
    public String[][] similarThree(String word){
        char[] alphabet = "abcdefghijklmnopqrstuvwxyzæøå".toCharArray();
        String str_alphabet = new String(alphabet);
        char[] word_array = word.toCharArray();
        char[] tmp;
        String[][] words = new String[word_array.length+1][alphabet.length];
        for(int i = 0; i < word_array.length+1; i++){
            for(int j=0; j< alphabet.length ; j++){
                tmp = word_array.clone();
                words[i][j] = replace2(i, alphabet[j], tmp);
            }
        }
        return words;
    }
    public String replace2(int a, char b, char [] word){
        char[] newWord=new char[word.length+1];
        newWord[a]=b;
        for(int i=0;i<a; i++){
            newWord[i]=word[i];
        }
        for(int j=newWord.length-1; j>a; j--){
            newWord[j]=word[j-1];
        }
        return new String (newWord);
    }//slutt Similar3






    //Similar 4 fjerde X
    public String[] similarFour(String word){
        char[] word_array = word.toCharArray();
        char[] tmp;
        String[] words = new String[word_array.length];
        for(int i = 0; i < word_array.length; i++){
            tmp = word_array.clone();
            words[i] = replace3(i, tmp);

        }
        return words;
    }
    public String replace3(int a, char [] word){
        char[] newWord=new char[word.length-1];
        int t=0;
        for(int i=0;i<word.length; i++){
            if(i!=a){
                newWord[t]=word[i];
                t++;
            }
        }
        return new String (newWord);
    }//Slutt similar 4








    void spellCheck(String strC){
        boolean b;
        b=exists(root, strC);
        if(b){
            System.out.println("Fant ord du letet etter: "+ strC);
        }else{
            System.out.println("Ordet " + strC+" er ikke i ordbok");
            System.out.println("Mente du?");
            checkAllSimilar(strC);
        }
    }



    void checkAllSimilar(String strC){
        long time1= System.nanoTime();

        String[] sim1=similarOne(strC);
        String[][] sim2=similarTwo(strC);
        String[][] sim3=similarThree(strC);
        String[] sim4=similarFour(strC);
        boolean b;
        int antExists=0;

        //sjekk sim1
        for(int i=0; i<sim1.length; i++){
            b=exists(root,sim1[i]);
            if(b){
                antExists++;
                print(sim1[i]);
            }
        }

        //Sjekk sim2
        for(int i=0; i<sim2.length; i++){
            for(int j=0; j<sim2[i].length; j++){
                b=exists(root,sim2[i][j]);
                if(b){
                    antExists++;
                    print(sim2[i][j]);
                }
            }
        }
        //sjekk sim3
        for(int i=0; i<sim3.length; i++){
            for(int j=0; j<sim3[0].length; j++){
                b=exists(root,sim3[i][j]);
                if(b){
                    antExists++;
                    print(sim3[i][j]);
                }
            }
        }

        //sjekk sim4
        for(int i=0; i<sim4.length; i++){
            b=exists(root,sim4[i]);
            if(b){
                antExists++;
                print(sim4[i]);
            }
        }

        long time2 = System.nanoTime();
        long timeSpent = time2-time1;
        System.out.println("Tilsammen er det "+antExists+" ord og brukt "+ timeSpent +" micro sec" );


    }//simkjekk end

    //dette er bare tull =)
    public void print(String str){
        System.out.println(str);
    }






    public void findDepths(Node node, int depth){

        if(node.left!=null){
            findDepths(node.left, depth++);
        }
        if(node.right!=null){
            findDepths(node.right, depth++);
        }
        if(node.left==null && node.right==null){
            antDybde++;
            arrDybde= Arrays.copyOf(arrDybde, antDybde+1);
            arrDybde[antDybde]=depth;
            //System.out.println(depth+ " tt");

        }
    }




    // Trenger bare å return'e size fordi vi øker size hver gang vi legger til en ny node (se
    // i insert) og vi reduserer size hver gang vi fjerner en node (se i delete).
    public int amountOfNodes() {
        return size;
    }

    public void findDeepest(){
        maxDybde=0;
        for(int i=0; i<arrDybde.length; i++){
            if(arrDybde[i]>maxDybde){
                maxDybde=arrDybde[i];
            }
        }
    }
    public int findAverageD(){
        for(int i=0; i<arrDybde.length; i++){sumDybde+=arrDybde[i];}
        return sumDybde/arrDybde.length;
    }
    public void finalWord(Node node){
        if(node.right!=null){
            finalWord(node.right);
        }else {finalW=node.verdi;}
    }

    public void firstWord(Node node){
        if(node.left!=null){
            firstWord(node.left);
        }else{firstW=node.verdi;}
    }






    void skrivTilFil(){
        FileWriter fw = null;
        try{
            fw=new FileWriter("utskrift.txt", false);
            fw.write("Statistikk\n");
            findDepths(root, 0);
            findDeepest();
            firstWord(root);
            finalWord(root);
            fw.write("Antal dybder i treet er  " + arrDybde.length+"\n");
            fw.write("Den dypeste node er på " + maxDybde +"\n");
            fw.write("Gjennomsnittelig dybde er  " + findAverageD()+ "\n");
            fw.write("Første ord i ordbok er  " + firstW+"\n");
            fw.write("Siste ord i ordbok er " + finalW+"\n");

            String spw[]={"etterfølger", "eterfølger", "etterfolger",
                    "etterfølgern", "tterfølger"};

            for(int p=0; p<5; p++){
                boolean check=exists(root, spw[p]);
                fw.write("Spellcheck for: "+spw[p]+"\n");
                if(check==false){
                    String[] sim1=similarOne(spw[p]);
                    String[][] sim2=similarTwo(spw[p]);
                    String[][] sim3=similarThree(spw[p]);
                    String[] sim4=similarFour(spw[p]);
                    boolean b;
                    int antExists=0;

                    //sjekk sim1
                    for(int i=0; i<sim1.length; i++){
                        b=exists(root,sim1[i]);
                        if(b){
                            antExists++;
                            fw.write(sim1[i]+"\n");

                        }
                    }

                    //Sjekk sim2
                    for(int i=0; i<sim2.length; i++){
                        for(int j=0; j<sim2[i].length; j++){
                            b=exists(root,sim2[i][j]);
                            if(b){
                                antExists++;
                                fw.write(sim2[i][j]+"\n");
                            }
                        }
                    }
                    //sjekk sim3
                    for(int i=0; i<sim3.length; i++){
                        for(int j=0; j<sim3[0].length; j++){
                            b=exists(root,sim3[i][j]);
                            if(b){
                                antExists++;
                                fw.write(sim3[i][j]+"\n");
                            }
                        }
                    }

                    //sjekk sim4
                    for(int i=0; i<sim4.length; i++){
                        b=exists(root,sim4[i]);
                        if(b){
                            antExists++;
                            fw.write(sim4[i]+"\n");
                        }
                    }
                    fw.write("Tilsammen er det "+antExists+" ord for " + spw[p]+"\n");

                }else {
                    fw.write(spw[p]+" finnes i ordbok\n");
                }
            }
            fw.close();
        }catch(Exception e){
            System.out.println("ikke lagret");
        }

    }

    //TREET

}
